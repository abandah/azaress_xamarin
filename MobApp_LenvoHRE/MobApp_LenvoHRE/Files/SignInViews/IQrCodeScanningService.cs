﻿using System.Threading.Tasks;

namespace MobApp_LenvoHRE.Files.SignInViews
{
	public interface IQrCodeScanningService
	{
		Task<string> ScanAsync();
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms.GoogleMaps;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Geolocator.Abstractions;
using MobApp_LenvoHRE.Files.Models;
using MobApp_LenvoHRE.Files.Refit;
using MobApp_LenvoHRE.Files.constant;
using Acr.UserDialogs;
using MobApp_LenvoHRE.Files.Service;

namespace MobApp_LenvoHRE.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Map : ContentPage
	{
		private Xamarin.Forms.GoogleMaps.Position position;
		private List<ATM> list;
		private int typeID;
		private string typeName;

		public Map()
		{
		}

		public Map(Xamarin.Forms.GoogleMaps.Position position)
		{
			InitializeComponent();
			this.position = position;
			StartMapWithMyLocation(position);

		}
		public void Punch_Clicked(object o, EventArgs e)
		{

			if (Util.Validation.IsConnected())
			{
				punchWithLocation();
			}
			else
			{
				shdaAsync();
			}

		}
		//Clutch_Clicked
		public void Clutch_Clicked(object o, EventArgs e)
		{
			if (Util.Validation.IsConnected())
			{
				StartLaunchPage();
			}
			else
			{
				shdaAsync();
			}
		}

		private async void shdaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{
				StartMapWithMyLocation(this.position);
			}
			else
			{
				//App.Current.MainPage = new SelectType_Page();
			}
		}

		public void StartLaunchPage()
		{
			try
			{
				var nav = (App.Current.MainPage as MasterDetailPage).Detail.Navigation;
				nav.PopModalAsync();
			}
			catch
			{
				(App.Current.MainPage as NavigationPage).Navigation.PopModalAsync();
			}
		}
		private async void punchWithLocation()
		{
			var locator = CrossGeolocator.Current;
			var location = await locator.GetPositionAsync();
			position = new Xamarin.Forms.GoogleMaps.Position(location.Latitude, location.Longitude);
			StartCall<Object>(position.Latitude + "", position.Longitude + "", typeID + "");
		}

		public Map(List<ATM> list, int typeID, string typeName)
		{
			InitializeComponent();
			this.list = list;
			this.typeID = typeID;
			this.typeName = typeName;

			foreach (ATM atm in list)
			{
				makeCircle(atm.GPSLatitude, atm.GPSLongitude, atm.GPSRadius, atm.ID + "");
				addPin(atm.GPSLatitude, atm.GPSLongitude, atm.ID + " - " + atm.MachineName);
			}

			btn.Text = Helpers.TranslateExtension.Translate("Punch")+ "(" + typeName + ")";
			setLocationZoomToCurrent();
			Start();


		}

		private async void setLocationZoomToCurrent()
		{
			position = await LocationGetter.getlocation();
			if(position.Latitude==0 || position.Longitude == 0)
			{
				GetLocation();
			}
			else
			{
				StartMapWithMyLocation(position);
				moveCamera(position.Latitude,position.Longitude);
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

		}
		private async void StartMapWithMyLocation(Xamarin.Forms.GoogleMaps.Position position)
		{
			//Xamarin.Forms.GoogleMaps.Position position = await GetLocation();
			map.MyLocationEnabled = true;
			map.UiSettings.MyLocationButtonEnabled = true;
			moveCamera(position.Latitude, position.Longitude);

		}

		public void addPin(double lat, double longt, string tag)
		{
			var pin = new Pin
			{
				Position = new Xamarin.Forms.GoogleMaps.Position(lat, longt),
				Label = tag,
				Tag = tag
			};
			map.Pins.Add(pin);
		}
		public void moveCamera(double lat, double longt)
		{
			map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(lat, longt),
														 Distance.FromMeters(150)));

		}
		public async void GetLocation()
		{

			var locator = CrossGeolocator.Current;
			var location = await locator.GetPositionAsync();
			position = new Xamarin.Forms.GoogleMaps.Position(location.Latitude, location.Longitude);
			StartMapWithMyLocation(position);


		}

		public void makeCircle(double lat, double longt, int Radius, string Tag)
		{

			Circle circle = new Circle();
			circle.IsClickable = true;
			circle.Center = new Xamarin.Forms.GoogleMaps.Position(lat, longt);
			circle.Radius = Distance.FromMeters(Radius);

			circle.StrokeColor = Color.Purple;
			circle.StrokeWidth = 6f;
			circle.FillColor = Color.FromRgba(0, 0, 255, 32);
			circle.Tag = Tag; // Can set any object
			circle.Clicked += (sender, e) =>
			{
				Circle circle2 = (Circle)sender;
				DisplayAlert("Circle", $"{(string)circle2.Tag} Clicked!", "Close");
			};

			map.Circles.Add(circle);

		}
		private async void Start()
		{
			var status = PermissionStatus.Granted;
			if (status == PermissionStatus.Granted)
			{
				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = WSConstants.LOCATION_ACCURACY;


				if (locator.IsGeolocationEnabled == false)
				{
					await DisplayAlert(Helpers.TranslateExtension.Translate("Need_location"), Helpers.TranslateExtension.Translate("Please_turn_on_your_device_location_to_enable_punch"), Helpers.TranslateExtension.Translate("OK"));
					//	await Navigation.PushAsync(new NeedLocation());
					return;
				}

				//if (CrossGeolocator.Current.IsListening)
				//return;
				try
				{
					await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(1), 100, true);

					CrossGeolocator.Current.PositionChanged += PositionChanged;
					CrossGeolocator.Current.PositionError += PositionError;
				}
				catch
				{
				}
				//StartCall<EntryTypes>();

				//lat.text = position.latitude.tostring();
				//lng.text = position.longitude.tostring();
				//mymap.movetoregion(mapspan.fromcenterandradius(new position(position.latitude, position.longitude), distance.frommeters(150)));

			}
			else if (status != PermissionStatus.Unknown)
			{
				await DisplayAlert(Helpers.TranslateExtension.Translate("Location_Denied"), Helpers.TranslateExtension.Translate("Permission_unknown_state"), Helpers.TranslateExtension.Translate("OK"));
				return;
			}


		}

		private void PositionError(object sender, PositionErrorEventArgs e)
		{
			DisplayAlert(Helpers.TranslateExtension.Translate("Location_Denied"), Helpers.TranslateExtension.Translate("Can_not_continue"), Helpers.TranslateExtension.Translate("OK"));
		}

		private void PositionChanged(object sender, PositionEventArgs e)
		{
			Util.ProgressDialog.HideProgressDialog();

			moveCamera(e.Position.Latitude, e.Position.Longitude);
		}
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			CrossGeolocator.Current.StopListeningAsync();

		}
		public void StartCall<T>(string LAT, string LONG, string TypeID)
		{
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, Global.UserId },
				{ WSConstants.PARAM_LENVO_TOKEN, Global.getLenvoLoginToken() },
				{ WSConstants.PARAM_UserLatitude, LAT },
				{ WSConstants.PARAM_UserLongitude, LONG },
				{ WSConstants.PARAM_EntryType, TypeID }
			};

			CallAPI<T> c = new CallAPI<T>();
			c.DataResponse += (sender, e) =>
			{
				try
				{
					if (e.Status == 1)
					{
						DisplayAlert(Helpers.TranslateExtension.Translate("Success"), e.Message, "OK");
						Clutchbtn.Text = Helpers.TranslateExtension.Translate("Finish");
					}
					else
					{
						DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), e.Message, Helpers.TranslateExtension.Translate("OK"));

					}
				}catch
				{
				}
			};
			c.CallApi_GetResponce(PunchByGPSLocation<T>, data);
		}
		public async Task<WSResponse<T>> PunchByGPSLocation<T>(Dictionary<string, string> data)
		{
			LenvoJobsAPIs<T> interfac = CallApi.Caller<LenvoJobsAPIs<T>>(Global.Link + WSConstants.API_URL);
			WSResponse<T> wSResponse = await interfac.PunchByGPSLocation(data);
			return wSResponse;
		}
	}
}
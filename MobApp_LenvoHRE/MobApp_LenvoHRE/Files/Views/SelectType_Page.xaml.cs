﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using MobApp_LenvoHRE.Files.constant;
using MobApp_LenvoHRE.Files.Interfaces;
using MobApp_LenvoHRE.Files.Models;
using MobApp_LenvoHRE.Files.Refit;
using MobApp_LenvoHRE.Files.Service;
using Plugin.Geolocator;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LenvoHRE.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectType_Page : ContentPage
	{
		private List<EntryTypes> list;

		public SelectType_Page()
		{
			InitializeComponent();

		}
		protected override bool OnBackButtonPressed()
		{
			return base.OnBackButtonPressed();
		}

		public void fillMenu(List<EntryTypes> list)
		{
			StackLayout sltitle = new StackLayout();
			sltitle.Orientation = StackOrientation.Vertical;

			StackLayout sl = new StackLayout();
			sl.Margin = 30;

			Button btntitle = new Button();
			btntitle.Text = Helpers.TranslateExtension.Translate("Back");
			btntitle.TextColor = Color.FromHex("#000000");
			btntitle.FontSize = 20;
			btntitle.VerticalOptions = LayoutOptions.End;
			btntitle.HorizontalOptions = LayoutOptions.CenterAndExpand;
			btntitle.Clicked += BackBtnClick;

			Label lbltitle = new Label();
			lbltitle.BackgroundColor = Color.FromHex("#F26827");
			lbltitle.Text = Helpers.TranslateExtension.Translate("Punch_Type");
			lbltitle.HeightRequest = 60;
			lbltitle.TextColor = Color.FromHex("#FFFFFF");
			lbltitle.VerticalTextAlignment = TextAlignment.Center;
			lbltitle.HorizontalTextAlignment = TextAlignment.Center;
			lbltitle.FontSize = 20;

			for (int i = 0; i < list.Count; i++)
			{
				EntryTypes it = list[i];

				Button btn = new Button();
				btn.Text = it.TypeName;
				btn.BackgroundColor = Color.FromHex("#F26827");
				btn.Margin = 10;
				btn.TextColor = Color.FromHex("#FFFFFF");
				btn.Clicked += (sender, e) =>
				 {
					 if (Util.Validation.IsConnected())
					 {
						 Util.ProgressDialog.ShowProgressDialog();

						 EntryTypes entry = it;

                         _ = haveAsync(entry.TypeID, entry.TypeName);
					 }
					 else
					 {
						 shdaAsync();
					 }

				 };

				sl.Children.Add(btn);

				
				sltitle.Children.Add(lbltitle);
				sltitle.Children.Add(sl);
				if (Device.OS == TargetPlatform.iOS)
				{
					sltitle.Children.Add(btntitle);
				}
			}
			myContentLayout.Content = sltitle;
		}

		private async void shdaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{
				fillMenu(list);
			}
			else
			{
				//App.Current.MainPage = new MainPage();
			}
		}

		private void BackBtnClick(object sender, EventArgs e)
		{
			try
			{
				var nav = (App.Current.MainPage as MasterDetailPage).Detail.Navigation;
				nav.PopModalAsync();
			}
			catch
			{
				(App.Current.MainPage as NavigationPage).Navigation.PopModalAsync();
			}
		}

		private async void ShowExitDialog()
		{
			var answer = await DisplayAlert(Helpers.TranslateExtension.Translate("Warning"), Helpers.TranslateExtension.Translate("There_is_no_available_GPS_punch_for_you_would_you_like_to_cancel_punching"), Helpers.TranslateExtension.Translate("Yes"), Helpers.TranslateExtension.Translate("No"));
			if (answer)
			{
				OnBackButtonPressed();
			}
		
		}

		private async Task haveAsync(int typeID, string typeName)
		{
            Xamarin.Forms.GoogleMaps.Position position;

            if (LocationGetter.IsLocationEmpty())
			{
				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 100;

				Plugin.Geolocator.Abstractions.Position location = await locator.GetLastKnownLocationAsync();


				if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
				{
					Util.ProgressDialog.HideProgressDialog();
                    return;
				}
				if (location == null) {
					Location l = await Geolocation.GetLastKnownLocationAsync();
					if (l != null)
					{
						location = new Plugin.Geolocator.Abstractions.Position(l.Latitude, l.Longitude);
					}
				}
				if (location == null)
				{
					//locator.DesiredAccuracy = 50;
					//location = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
					//position = new Xamarin.Forms.GoogleMaps.Position(location.Latitude, location.Longitude);
					var request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(10));

					Location l = await Geolocation.GetLocationAsync(request);
					if (l != null)
					{
						if (location.IsFromMockProvider)
						{
							// location is from a mock provider
						}
						location = new Plugin.Geolocator.Abstractions.Position(l.Latitude, l.Longitude);
					}

                }
				if (location == null)
				{
					Util.ProgressDialog.ShowProgressDialog();
					Util.ProgressDialog.HideProgressDialog();

					return;
				}
				else
				{
					position = new Xamarin.Forms.GoogleMaps.Position(location.Latitude, location.Longitude);
				}
			}
			else
			{
				position = await LocationGetter.getlocation();
			}
			StartCall<ATM>(position.Latitude + "", position.Longitude + "", typeID, typeName);
		}

		public SelectType_Page(List<EntryTypes> list)
		{
			InitializeComponent();
			this.list = list;
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			fillMenu(list);

			ItemListViewModel itemListViewModel;
			itemListViewModel = new ItemListViewModel(list);
			BindingContext = itemListViewModel;
		}

		public void StartCall<T>(string LAT, string LONG, int TypeID, string typeName)
		{
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, Global.UserId },
				{ WSConstants.PARAM_LENVO_TOKEN, Global.getLenvoLoginToken() },
				{ WSConstants.PARAM_Latitude, LAT },
				{ WSConstants.PARAM_Longitude, LONG },};

			CallAPI<T> c = new CallAPI<T>();
			c.DataResponse += (sender, e) =>
			{

				if (e.Status == 1)
				{
					//App.Current.MainPage = new NavigationPage(new Map());
					if (e.dataFromServer == null) {
						ShowExitDialog();
						}
					else
					{
						try
						{
							(App.Current.MainPage as MasterDetailPage).Detail.Navigation.PushModalAsync(new Map(e.dataFromServer.DataRows.Cast<ATM>().ToList(), TypeID, typeName));
						}
						catch
						{
							App.Current.MainPage.Navigation.PushModalAsync(new Files.Views.Map(e.dataFromServer.DataRows.Cast<ATM>().ToList(), TypeID, typeName));
						}
						//Util.ProgressDialog.ShowProgressDialog();
					}
					//DisplayAlert("Success", "Success", "OK!");
				}
				else if (e.Status == 0)
				{
					DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), e.Message, Helpers.TranslateExtension.Translate("OK"));
				}
				else
				{
					DisplayAlert(Helpers.TranslateExtension.Translate("Error"), e.Message, Helpers.TranslateExtension.Translate("OK"));

				}
			};
			c.CallApi_GetResponce(GetClosestGPSATMs<T>, data);
		}
		public async Task<WSResponse<T>> GetClosestGPSATMs<T>(Dictionary<string, string> data)
		{
			LenvoJobsAPIs<T> interfac = CallApi.Caller<LenvoJobsAPIs<T>>(Global.Link + WSConstants.API_URL);
			WSResponse<T> wSResponse = await interfac.GetClosestGPSATMs(data);
			return wSResponse;
		}
	}
}
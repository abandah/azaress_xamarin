﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LenvoHRE.Files.Models
{
    public class CarouselData
    {
        public string Name { get; set; }
        public object Image { get; set; }
    }
}

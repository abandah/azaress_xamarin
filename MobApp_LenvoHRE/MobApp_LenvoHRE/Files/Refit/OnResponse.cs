﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LenvoHRE.Files.Refit
{
    interface OnResponse<T>
    {
       void OnResult(int CallId, WSResponse<T> response);
        
    }
}

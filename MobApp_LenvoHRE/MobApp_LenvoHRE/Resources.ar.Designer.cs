﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MobApp_LenvoHRE {
    using System;
    using System.Reflection;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources_ar {
        
        private static System.Resources.ResourceManager resourceMan;
        
        private static System.Globalization.CultureInfo resourceCulture;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources_ar() {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public static System.Resources.ResourceManager ResourceManager {
            get {
                if (object.Equals(null, resourceMan)) {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("MobApp_LenvoHRE.Resources.ar", typeof(Resources_ar).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public static System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        public static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        public static string By_Tapping_Log_In_you_agree_to_our {
            get {
                return ResourceManager.GetString("By_Tapping_Log_In_you_agree_to_our", resourceCulture);
            }
        }
        
        public static string Camera_Denied {
            get {
                return ResourceManager.GetString("Camera_Denied", resourceCulture);
            }
        }
        
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        public static string Can_not_continue {
            get {
                return ResourceManager.GetString("Can_not_continue", resourceCulture);
            }
        }
        
        public static string Close {
            get {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }
        
        public static string Enter_your_username_or_Id_and_the_password_then_tap_the_QR_code_image_to_scan_the_QR_code_from_your_desktop_profile_under_Mobile_access_tab {
            get {
                return ResourceManager.GetString("Enter_your_username_or_Id_and_the_password_then_tap_the_QR_code_image_to_scan_the" +
                        "_QR_code_from_your_desktop_profile_under_Mobile_access_tab", resourceCulture);
            }
        }
        
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        public static string Exit {
            get {
                return ResourceManager.GetString("Exit", resourceCulture);
            }
        }
        
        public static string ExitApp {
            get {
                return ResourceManager.GetString("ExitApp", resourceCulture);
            }
        }
        
        public static string Failed {
            get {
                return ResourceManager.GetString("Failed", resourceCulture);
            }
        }
        
        public static string Finish {
            get {
                return ResourceManager.GetString("Finish", resourceCulture);
            }
        }
        
        public static string LenvoHRE_needs_camera_access {
            get {
                return ResourceManager.GetString("LenvoHRE_needs_camera_access", resourceCulture);
            }
        }
        
        public static string LenvoHRE_needs_to_access_your_location_in_order_to_place_a_punch_in_your_attendance_log {
            get {
                return ResourceManager.GetString("LenvoHRE_needs_to_access_your_location_in_order_to_place_a_punch_in_your_attendan" +
                        "ce_log", resourceCulture);
            }
        }
        
        public static string Logout {
            get {
                return ResourceManager.GetString("Logout", resourceCulture);
            }
        }
        
        public static string Manual_Login {
            get {
                return ResourceManager.GetString("Manual_Login", resourceCulture);
            }
        }
        
        public static string Menu {
            get {
                return ResourceManager.GetString("Menu", resourceCulture);
            }
        }
        
        public static string Name_is_required {
            get {
                return ResourceManager.GetString("Name_is_required", resourceCulture);
            }
        }
        
        public static string Need_Camera {
            get {
                return ResourceManager.GetString("Need_Camera", resourceCulture);
            }
        }
        
        public static string Need_location {
            get {
                return ResourceManager.GetString("Need_location", resourceCulture);
            }
        }
        
        public static string Next {
            get {
                return ResourceManager.GetString("Next", resourceCulture);
            }
        }
        
        public static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        public static string No_Internet_Connection {
            get {
                return ResourceManager.GetString("No_Internet_Connection", resourceCulture);
            }
        }
        
        public static string Ok {
            get {
                return ResourceManager.GetString("Ok", resourceCulture);
            }
        }
        
        public static string Password_is_required {
            get {
                return ResourceManager.GetString("Password_is_required", resourceCulture);
            }
        }
        
        public static string Permission_unknown_state {
            get {
                return ResourceManager.GetString("Permission_unknown_state", resourceCulture);
            }
        }
        
        public static string Please_turn_on_your_device_location_to_enable_punch {
            get {
                return ResourceManager.GetString("Please_turn_on_your_device_location_to_enable_punch", resourceCulture);
            }
        }
        
        public static string Punch {
            get {
                return ResourceManager.GetString("Punch", resourceCulture);
            }
        }
        
        public static string Punch_Type {
            get {
                return ResourceManager.GetString("Punch_Type", resourceCulture);
            }
        }
        
        public static string QRCode_is_required {
            get {
                return ResourceManager.GetString("QRCode_is_required", resourceCulture);
            }
        }
        
        public static string Retry {
            get {
                return ResourceManager.GetString("Retry", resourceCulture);
            }
        }
        
        public static string Scan_QRCode {
            get {
                return ResourceManager.GetString("Scan_QRCode", resourceCulture);
            }
        }
        
        public static string Scan_QR_code {
            get {
                return ResourceManager.GetString("Scan_QR_code", resourceCulture);
            }
        }
        
        public static string Settings {
            get {
                return ResourceManager.GetString("Settings", resourceCulture);
            }
        }
        
        public static string Success {
            get {
                return ResourceManager.GetString("Success", resourceCulture);
            }
        }
        
        public static string Terms_and_Privacy_Policy {
            get {
                return ResourceManager.GetString("Terms_and_Privacy_Policy", resourceCulture);
            }
        }
        
        public static string There_is_no_available_GPS_punch_for_you_would_you_like_to_cancel_punching {
            get {
                return ResourceManager.GetString("There_is_no_available_GPS_punch_for_you_would_you_like_to_cancel_punching", resourceCulture);
            }
        }
        
        public static string There_is_no_internet_connection {
            get {
                return ResourceManager.GetString("There_is_no_internet_connection", resourceCulture);
            }
        }
        
        public static string Warning {
            get {
                return ResourceManager.GetString("Warning", resourceCulture);
            }
        }
        
        public static string Yes {
            get {
                return ResourceManager.GetString("Yes", resourceCulture);
            }
        }
    }
}

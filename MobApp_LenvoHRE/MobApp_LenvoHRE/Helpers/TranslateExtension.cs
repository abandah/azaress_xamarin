﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LenvoHRE.Helpers
{
	[ContentProperty("Text")]
	public class TranslateExtension : IMarkupExtension
	{
		public string Text { get; set; }

		public object ProvideValue(IServiceProvider serviceProvider)
		{
			if (Text == null)
				return null;

			var assembly = typeof(TranslateExtension).GetTypeInfo().Assembly;
			var assemblyName = assembly.GetName();
			ResourceManager resourceManager = new ResourceManager($"{assemblyName.Name}.Resources", assembly);

			String s =  resourceManager.GetString(Text, CultureInfo.CurrentCulture);
			return s;
		}
		public static string Translate(string Text)
		{
			if (Text == null)
				return null;

			var assembly = typeof(TranslateExtension).GetTypeInfo().Assembly;
			var assemblyName = assembly.GetName();
			ResourceManager resourceManager = new ResourceManager($"{assemblyName.Name}.Resources", assembly);

			String s = resourceManager.GetString(Text, CultureInfo.CurrentCulture);
			if (s == null)
				return Text;
			return s;
		}
	}
}